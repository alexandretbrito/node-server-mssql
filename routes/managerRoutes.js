const express = require('express');
const router = express.Router();
const dbChain = require('../queries/managerQueries');

/// ADMINISTRADORES

router.get('/listaadmin', async(req, res, next) => {
    const result = await dbChain.loadManagers() 
    res.send(result)
})

router.post('/client_table_admin', async(req, res) => {
    const result = await dbChain.loadClientManagers(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})

router.post('/insereadmin', async(req, res) => {
    const result = await dbChain.adminInsertion(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})

module.exports = router;