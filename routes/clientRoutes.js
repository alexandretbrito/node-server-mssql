const express = require('express');
const router = express.Router();
const dbChain = require('../queries/clientQueries');

/// CLIENTES

router.get('/listaclientes', async(req, res, next) => {
    const result = await dbChain.loadClients() 
    res.send(result)
})

router.get('/detalhecliente', async(req, res, next) => {
    const result = await dbChain.loadClientsDetails() 
    res.send(result)
})

router.get('/pegacliente', async(req, res, next) => {
    const result = await dbChain.getClient(req.query) 
    res.send(result)
})

router.post('/deletacliente', async(req, res, next) => {
    const result = await dbChain.deleteClient(req.body) 
    res.send(result)
})

router.post('/inserecliente', async(req, res) => {
    const result = await dbChain.clientInsertion(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})


module.exports = router;