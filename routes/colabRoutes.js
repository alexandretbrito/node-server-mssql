const express = require('express');
const router = express.Router();
const dbChain = require('../queries/driverqueries');

router.get('/roles', async(req, res, next) => {
    const result = await dbChain.loadColabRoles() 
    res.send(result)
})

router.get('/workers', async(req, res) => {
    const result = await dbChain.loadColaborators(req.query) 
    res.send(result)
})

router.get('/oneworker', async(req, res) => {
    const result = await dbChain.loadTheColaborator(req.query) 
    res.send(result)
})

router.post('/suspendworker', async(req, res) => {
    const result = await dbChain.suspendTheColaborator(req.body) 
    res.send(result)
})

router.post('/updateworker', async(req, res) => {
    const result = await dbChain.updateColaborator(req.body) 
    res.send(result)
})

router.post('/deleteworker', async(req, res) => {
    const result = await dbChain.deleteColaborator(req.body) 
    res.send(result)
})

router.post('/inseremotorista', async(req, res) => {
    const result = await dbChain.colabInsertion(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})

module.exports = router;
