const express = require('express');
const router = express.Router();
const dbChain = require('../queries/appQueries');

/** LOGIN */
//Cliente por PIN
router.get('/clientpin', async(req, res, next) => {
    const result = await dbChain.getClientByAPI(req.query) 
    res.send(result)
})
//Motorista por PIN
router.get('/driverpin', async(req, res, next) => {
    const result = await dbChain.getColabByAPI(req.query) 
    res.send(result)
})
/** MOTORISTA */
//Jornada (motorista) início
router.post('/inserejornada', async(req, res) => {
    console.log(req.body);
    const result = await dbChain.startJourney(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})
//Jornada (motorista) final
router.post('/terminajornada', async(req, res) => {
    console.log(req.body);
    const result = await dbChain.finishJourney(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})
//Jornada (motorista) macros diversas
router.post('/inseremacro', async(req, res) => {
    console.log(req.body);
    const result = await dbChain.startMacro(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})
//Jornada (motorista) todas as jornadas do motorista
router.get('/pegajornadas', async(req, res, next) => {
    const result = await dbChain.getOldJourneys(req.query) 
    res.send(result)
})
//Jornada (motorista) inserir o Tracking
router.post('/inseretracking', async(req, res) => {
    console.log(req.body);
    const result = await dbChain.trackInsertion(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})
/** PONTO */
//Ponto (colaborador) dias de trabalho anteriores
router.get('/pontosdecolaborador', async(req, res, next) => {
    const result = await dbChain.getPastWorkDays(req.query) 
    res.send(result)
})
//Ponto (colaborador) início do dia
router.post('/iniciaponto', async(req, res) => {
    console.log(req.body);
    const result = await dbChain.startWorkDay(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})
//Ponto (colaborador) final do dia
router.post('/fechaponto', async(req, res) => {
    //console.log(req.body);
    const result = await dbChain.finishWorkDay(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})
//Ponto (colaborador) começo do almoço
router.post('/abrealmoco', async(req, res) => {
    //console.log(req.body);
    const result = await dbChain.startLunch(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})
//Ponto (colaborador) fim do almoço
router.post('/fimdoalmoco', async(req, res) => {
    //console.log(req.body);
    const result = await dbChain.finishLunch(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})

/**  CADASTRO DE CLIENTE PELO APP */
router.post('/cadastrateste', async(req, res) => {
    const result = await dbChain.clientTestRegistration(req.body);
    if(result)  {
        console.log("recebeu");
        res.send(result)
    }else{
        console.log("Um erro aconteceu")
        res.send({data: "Um erro aconteceu"})
    }
})

/**  CADASTRO DE MAOTRISTA PELO APP */
router.post('/cadastramotorista', async(req, res) => {
    const result = await dbChain.driverRegistration(req.body);
    if(result)  {
        console.log("recebeu");
        res.send(result)
    }else{
        console.log("Um erro aconteceu")
        res.send({data: "Um erro aconteceu"})
    }
})
module.exports = router;