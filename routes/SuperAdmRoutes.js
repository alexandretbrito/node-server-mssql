const express = require('express');
const router = express.Router();
const dbChain = require('../queries/superAdmQueries');


/// SUPER ADMINISTRADORES

router.get('/listasuperadms', async(req, res, next) => {
    const result = await dbChain.loadSuperAdms() 
    res.send(result)
})

router.post('/deletesuperadm', async(req, res, next) => {
    const result = await dbChain.deleteSuperAdm(req.body) 
    res.send(result)
})

router.post('/inseresuperadm', async(req, res) => {
    const result = await dbChain.superAdmInsertion(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})

module.exports = router;