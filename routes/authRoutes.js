const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const dbChain = require('../queries/queries');
const tokenMngr = require('../utils/tokenManager');
const bcrypt = require('bcrypt');

router.post('/admin', async(req, res) => {
    console.log(req.body)
    const result = await dbChain.adminLogin(req.body);
    if(result){
        console.log(result);
        const middleResult = result.recordset[0];
        const payloader = {
            email: middleResult.ManagerEmail,
            password: middleResult.ManagerPWD
        }
        const middleConst = middleResult.ManagerPWD;
        console.log(middleConst)
        const validPass = await bcrypt.compare(req.body.Password, middleConst);
        if (validPass) {
            const accessToken = tokenMngr.setToken;
            const refreshToken = tokenMngr.setRefreshToken;               
                middleResult.token = accessToken(payloader);
                middleResult.refreshToken = refreshToken(payloader);
                console.log(middleResult);
                res.send(middleResult);  

        } else {
            res.send({data: "Login inválido."})
        }
    }else{
        res.send({data: "Usuário não encontrado"})
    }
})

router.post('/superadm', async(req, res) => {
    const result = await dbChain.superAdmLogin(req.body);
    console.log(result)
    if (result) {
        const middleResultAdm = result.recordset[0];
        const payloader = {
            email: middleResultAdm.SuperAdmEmail,
            password: middleResultAdm.SuperAdmPWD
        }
        const middleConstAdm = middleResultAdm.SuperAdmPWD;
        console.log(middleConstAdm)
        const validPassAdm = await bcrypt.compare(req.body.SuperAdmPWD, middleConstAdm);
        if (validPassAdm){
            console.log(middleResultAdm);
            res.send(middleResultAdm);
        } else {
            res.send({data: "Login inválido."})
        }

    } else {
        res.send({data: "Usuário não encontrado."})
    }
})

router.post('/driver', async(req, res) => {
    const result = await dbChain.adminDriver(req.body);
    console.log(result)
    const middleResult = result.recordset[0];
    if(middleResult)  {
        if (middleResult.Colab_Active) {
            res.send(middleResult);
        } else {
            res.send({data: "Você já está removido do sistema."})
        }
    }else{
        res.send({data: `Usuário não encontrado. Tente de novo ou contate a empresa.`})
    }
})

module.exports = router;