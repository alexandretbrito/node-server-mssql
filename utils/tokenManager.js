const jwt = require('jsonwebtoken');
const chave = require('./keys');

const setToken = ( payload ) => {
    const theToken = jwt.sign( payload, chave.token_key, {expiresIn: '20s'} )
    return theToken;
}

const setRefreshToken = ( payload ) => {
    const refreshToken = jwt.sign( payload, chave.refresh_key, {expiresIn: '5m'} )
    return refreshToken;
}


module.exports = {
    setToken,
    setRefreshToken
}