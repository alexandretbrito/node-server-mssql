
const empresaPinGenerator = (theDate) => {
    const entrada = new Date(theDate);
    let mes = entrada.getMonth() +1;
    if (String(mes).length < 2) {
        mes = "0" + String(mes);
    }
    let ano = entrada.getFullYear();
    var val = String(Math.floor(1000 + Math.random() * 9999));
    const pinId = ano+"-"+mes+"-"+val;
    return pinId;
}

const driverPinGenerator = () => {
    var val = String(Math.floor(1000 + Math.random() * 9999));
    return val;
}

module.exports = {
    empresaPinGenerator,
    driverPinGenerator
}