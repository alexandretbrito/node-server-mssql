const nodemailer = require("nodemailer");

// envio de e-mails
const sendClientRegisterMail = (clientMail, clientPin, clientName) => {
    var transporter = nodemailer.createTransport({
        host: process.env.MAIL_SERVER,
        port: '465',
        secure: true,
        auth: {
          user: 'mailteste@alexandrebrito.com',
          pass: process.env.MAIL_PWD
        },
        tls: {
            rejectUnauthorized: false,
          },
      });
      
      var mailOptions = {
        from: 'Multiponto Seven <mailteste@alexandrebrito.com>',
        to: clientMail,
        subject: `Cadastro da empresa ${clientName} no app Fênix Finder`,
        html: `Olá</br>
                O PIN gerado pelo sistema Fênix Finder para a empresa ${clientName} é:</br>
                <h3>${clientPin}</h3>
                Guarde-o em um lugar seguro, pois será necessário para os próximos passos.
                Equipe FenixSat.`
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });   
}

const sendDriverRegisterMail = (driverMail, driverPin, driverName) => {
  var transporter = nodemailer.createTransport({
    host: process.env.MAIL_SERVER,
    port: '465',
    secure: true,
    auth: {
      user: 'mailteste@alexandrebrito.com',
      pass: process.env.MAIL_PWD
    },
      tls: {
          rejectUnauthorized: false,
        },
    });
    
    var mailOptions = {
      from: 'Multiponto Seven <mailteste@alexandrebrito.com>',
      to: driverMail,
      subject: `Cadastro de ${driverName} no app Multiponto ESA`,
      html: `Olá</br>
              O PIN gerado pelo sistema Multiponto ESA para o colaborador ${driverName} é:</br>
              <h3>${driverPin}</h3>
              Guarde-o em um lugar seguro, pois será necessário para os próximos passos.
              Equipe Multiponto ESA.`
    };
    
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });   
}

const sendColabSuspendingMail = (driverMail, driverPin, driverName) => {
  var transporter = nodemailer.createTransport({
    host: process.env.MAIL_SERVER,
    port: '465',
    secure: true,
    auth: {
      user: 'mailteste@alexandrebrito.com',
      pass: process.env.MAIL_PWD
    },
      tls: {
          rejectUnauthorized: false,
        },
    });
    
    var mailOptions = {
      from: 'Multiponto Seven <mailteste@alexandrebrito.com>',
      to: driverMail,
      subject: `Desativação do cadastro de ${driverName} no app Multiponto ESA`,
      html: `Olá, ${driverName}</br>
              Você se descadastrou do sistema Multiponto ESA.</br>
              Guarde o PIN ${driverPin} em um lugar seguro, caso seja necessário.</br>
              Equipe Multiponto ESA.`
    };
    
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });   
}

module.exports = {
    sendClientRegisterMail,
    sendDriverRegisterMail,
    sendColabSuspendingMail
}