const token_key = process.env.TOKEM_KEY;
const refresh_key = process.env.REFRESH_KEY

module.exports = {
    token_key,
    refresh_key
}