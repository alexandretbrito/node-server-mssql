const config = {
    server: process.env.DB_SERVER,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PWD,
    options: {
        encrypt: false,
        useUTC: true,
        trustServerCertificate: true,
        trustedConnection: false,
        enableArithAbort: true
      },
};

module.exports = config;