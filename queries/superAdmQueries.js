const config = require('./dbConfig');
const mssql = require('mssql');
const bcrypt = require('bcrypt');

  /// SUPERADMINS

  const loadSuperAdms = async() => {
    try {
      console.log("acerto")
      let pool = await mssql.connect(config)
      let admins = pool.request().query("SELECT * from ADM_SuperAdmin");
      return admins;
    } catch (error) {
      console.log(error);
    }
  }

  const deleteSuperAdm = async( Bye ) => {
    try {
      console.log(Bye);
      var pool = await mssql.connect(config);
      var admin = await pool.request().query(
        `DELETE FROM ADM_SuperAdmin WHERE SuperAdmID = '${Bye.SuperAdmID}'`
      );
      return admin;
    } catch (error) {
      console.log(error);
    }
  }  

  const superAdmInsertion = async( superObj ) => {
    try {
      const hashedPass = await bcrypt.hash(superObj.SuperAdmPWD, 10)
      var pool = await mssql.connect(config);
      var admin = await pool.request().query(
        `INSERT INTO ADM_SuperAdmin (
          SuperAdmName, SuperAdmPWD, SuperAdmEmail) VALUES (
          '${superObj.SuperAdmName}', '${hashedPass}', '${superObj.SuperAdmEmail}')`
      );
      return admin;
    } catch (error) {
      console.log(error);
    }
  }

  module.exports = {
    loadSuperAdms,
    superAdmInsertion,
    deleteSuperAdm
}