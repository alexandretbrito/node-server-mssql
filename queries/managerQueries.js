const config = require('./dbConfig');
const mssql = require('mssql');
const bcrypt = require('bcrypt');

/// ADMINISTRADORES

/** traz todos os admins da conta */
const loadManagers = async() => {
    try {
      let pool = await mssql.connect(config)
      let admins = pool.request().query("SELECT * from ADM_Manager");
      return admins;
    } catch (error) {
      console.log(error);
    }
  }

  /** traz um administrador específico da conta */
  const loadClientManagers = async (client) => {
    console.log(client);
    try {
        let pool = await mssql.connect(config)
        let admins = pool.request().query(`SELECT * from ADM_Manager WHERE ManagerClientID = ${client.ClientID}`);
        return admins;
      } catch (error) {
        console.log(error);
      }
  }
  
  /** serve pata SuperAdmins e clientes do DashBoard */
  const adminInsertion = async( adminObj ) => {
    try {
      const hashedPass = await bcrypt.hash(adminObj.ManagerPWD, 10);
      /** colocar a geração de PIN de empresa */
      var pool = await mssql.connect(config);
      var admin = await pool.request().query(
        `INSERT INTO ADM_Manager (
          ManagerName, ManagerSurname, ManagerTel, 
          ManagerEmail, ManagerPWD, ManagerRoleID, 
          ManagerClientID) VALUES (
          '${adminObj.ManagerName}', '${adminObj.ManagerSurname}', '${adminObj.ManagerTel}',
          '${adminObj.ManagerEmail}', '${hashedPass}', '${adminObj.ManagerRoleID}', 
          '${adminObj.ManagerClientID}')`
      );
      /** enviar email de sucesso */
      return admin;
    } catch (error) {
      console.log(error);
    }
  }



module.exports = {
    loadManagers,
    adminInsertion,
    loadClientManagers
}


