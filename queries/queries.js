const config = require('./dbConfig');
const mssql = require('mssql');
const bcrypt = require('bcrypt');



const loadBasic = async() => {
    try {
      console.log("acerto")
      let pool = await mssql.connect(config)
      let admins = pool.request().query("SELECT * from ADM_Manager");
      return admins;
    } catch (error) {
      console.log(error);
    }
  }

  /// ROLES

  const loadRoles = async() => {
    try {
      let pool = await mssql.connect(config)
      let admins = pool.request().query("SELECT * from ADM_Roles");
      return admins;
    } catch (error) {
      console.log(error);
    }
  }

  /// AUTENTICAÇÕES

  const adminLogin = async(logger) => {
    try {
      var pool = await mssql.connect(config);
      var admin = await pool.request().query( `SELECT * from ADM_Manager WHERE 
        ManagerEmail = '${logger.Email}'`);     
        return admin;
    } catch (error) {
      console.log(error);
    }
  }

  const superAdmLogin = async(admLogger) => {
    try {
      var pool = await mssql.connect(config);
      var askedAdm = await pool.request().query( `SELECT * from ADM_SuperAdmin WHERE 
      SuperAdmEmail = '${admLogger.SuperAdmEmail}'`);
        return askedAdm;
    } catch (error) {
      console.log(error);
    }
  }

  const adminDriver = async(logger) => {
    try {
      console.log(logger);
      var pool = await mssql.connect(config);
      var driver = await pool.request().query( `SELECT * from ADM_Colab WHERE 
        ColabEmail = '${logger.Email}' AND 
        ColabPIN_ID = '${logger.DriverPin}'`);   
        return driver;
    } catch (error) {
      console.log(error);
    }
  }



  /// SUPERADMINS

  const loadSuperAdms = async() => {
    try {
      console.log("acerto")
      let pool = await mssql.connect(config)
      let admins = pool.request().query("SELECT * from ADM_SuperAdmin");
      return admins;
    } catch (error) {
      console.log(error);
    }
  }

  const superAdmInsertion = async( superObj ) => {
    try {
      const hashedPass = await bcrypt.hash(superObj.SuperAdmPWD, 10)
      var pool = await mssql.connect(config);
      var admin = await pool.request().query(
        `INSERT INTO ADM_SuperAdmin (
          SuperAdmName, SuperAdmPWD, SuperAdmEmail) VALUES (
          '${superObj.SuperAdmName}', '${hashedPass}', '${superObj.SuperAdmEmail}')`
      );
      return admin;
    } catch (error) {
      console.log(error);
    }
  }

module.exports = {
    loadBasic,
    loadRoles,
    adminLogin,
    adminDriver,
    loadSuperAdms,
    superAdmInsertion,
    superAdmLogin
}