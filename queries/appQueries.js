const config = require('./dbConfig');
const mssql = require('mssql');
const pinGenerator = require('../utils/pinGenerator');
const emailSender = require('../utils/emailSender');

const getClientByAPI = async(api) => {
    console.log(api)
    try {        
        var pool = await mssql.connect(config)
        var clientepin = pool.request().query(`SELECT ClientID, ClientName, ClientActive FROM ADM_Client 
        WHERE ClientPIN = '${api.clientPin}'`);
        return clientepin;
      } catch (error) {
        console.log(error);
      }
    }

const getColabByAPI = async(corpo) => {
    try {
        var pool = await mssql.connect(config)
        var driverpin = pool.request().query(`SELECT ColabID,ColabRole_ID, ColabName, ColabSurname, ColabPIN_ID, Colab_Active FROM ADM_Colab 
        WHERE ColabPIN_ID = '${corpo.driverPin}' AND ColabClientID = '${corpo.ClientID}'`);
        return driverpin;
    } catch (error) {
        console.log(error);
    }
}

const startJourney = async(jornada) => {
    try {
        var pool = await mssql.connect(config);
        var journey = await pool.request().query(
            `INSERT INTO JRN_Jornada (ColabID, ClientID, JornStartTime)
            OUTPUT inserted.JornID
            VALUES ('${jornada.ColabID}', '${jornada.ClientID}', '${jornada.JornTime}')`
        );
        return journey;
    } catch (error) {
        console.log(error);
    }
}

const finishJourney = async(jornada) => {
    try {
        var pool = await mssql.connect(config);
        var journey = await pool.request().query(
            `UPDATE JRN_Jornada 
            SET JornFinalTime= '${jornada.JornTime}' 
            WHERE JornID = '${jornada.JornID}'`
        );
        return journey;
    } catch (error) {
        console.log(error);
    }
}

const startMacro = async(macroInfo) => {
    try {
        var pool = await mssql.connect(config);
        var macroIn = await pool.request().query(
            `INSERT INTO JRN_Macros (JornID, MacroType, MacroLat,
            MacroLong, MacroStartTime)
            VALUES ('${macroInfo.JornID}', '${macroInfo.MacroType}',
             '${macroInfo.JornLat}', '${macroInfo.JornLong}',
             '${macroInfo.JornTime}')`
        );
        return macroIn;    
    } catch (error) {
        console.log(error);
    }
}

const getOldJourneys = async(journeyDets) => {
    try {
        var pool = await mssql.connect(config);
        var oldJourneys = await pool.request().query(
            `SELECT * FROM JRN_Jornada WHERE ColabID = '${journeyDets.DriverID}' 
            AND JornFinalTime IS NOT NULL`
        );
        return oldJourneys;
    } catch (error) {
        console.log(error);
    }
}


const getPastWorkDays = async(WorkDets) => {
    try {
        var pool = await mssql.connect(config);
        var pastWorkDays = await pool.request().query(
            `SELECT * FROM ADM_Ponto WHERE ColabID = '${WorkDets.ColabID}' 
            AND PntFinalTime IS NOT NULL`
        );
        return pastWorkDays;
    } catch (error) {
        console.log(error);
    }
}

const startWorkDay = async(dia) => {
    try {
        var pool = await mssql.connect(config);
        var journey = await pool.request().query(
            `INSERT INTO ADM_Ponto (ColabID, PntStartTime, PntStartLat, PntStartLong)
            OUTPUT inserted.PntID
            VALUES ('${dia.ColabID}', '${dia.PntStartTime}', '${dia.PntStartLat}', '${dia.PntStartLong}')`
        );
        return journey;
    } catch (error) {
        console.log(error);
    }
}

const finishWorkDay = async(fim_dia) => {
    console.log(fim_dia);
    try {
        var pool = await mssql.connect(config);
        var end_day = await pool.request().query(
            `UPDATE ADM_Ponto 
            SET PntFinalTime='${fim_dia.PntFinalTime}', 
            PntFinalLat='${fim_dia.PntFinalLat}', 
            PntFinalLong='${fim_dia.PntFinalLong}'
            WHERE PntID = '${fim_dia.PntID}'`
        );
        return end_day;
    } catch (error) {
        console.log(error);
    }
}

const startLunch = async(rango) => {
    console.log(rango);
    try {
        var pool = await mssql.connect(config);
        var end_day = await pool.request().query(
            `UPDATE ADM_Ponto 
            SET PntLunchStart='${rango.PntLunchStart}', 
            PntLunchStartLat='${rango.PntLunchStartLat}', 
            PntLunchStartLong='${rango.PntLunchStartLong}'
            WHERE PntID = '${rango.PntID}'`
        );
        return end_day;
    } catch (error) {
        console.log(error);
    }
}

const finishLunch = async(rango) => {
    console.log(rango);
    try {
        var pool = await mssql.connect(config);
        var end_day = await pool.request().query(
            `UPDATE ADM_Ponto 
            SET PntLunchFinal='${rango.PntLunchFinal}', 
            PntLunchFinalLat='${rango.PntLunchFinalLat}', 
            PntLunchFinalLong='${rango.PntLunchFinalLong}'
            WHERE PntID = '${rango.PntID}'`
        );
        return end_day;
    } catch (error) {
        console.log(error);
    }
}

const trackInsertion = async(trackInfo) => {
    try {
        var pool = await mssql.connect(config);
        var trackIn = await pool.request().query(
            `INSERT INTO JRN_Tracking (JornID, TrackTime, TrackLatitude, TrackLongitude, TrackAccuracy, TrackAltitude, TrackAltitudeAccuracy, TrackHeading, TrackSpeed)
            VALUES ('${trackInfo.JornID}', '${trackInfo.time}',
             '${String(trackInfo.latitude)}', '${String(trackInfo.longitude)}', '${String(trackInfo.accuracy)}', 
             '${String(trackInfo.altitude)}',
             '${String(trackInfo.altitudeAccuracy)}', 
             '${String(trackInfo.heading)}', 
             '${String(trackInfo.speed)}')`
        );
        return trackIn;    
    } catch (error) {
        console.log(error);
    }
}

const clientTestRegistration = async(registerInfo) => {
    try {
        const thePin = {
            ClientPIN : pinGenerator.empresaPinGenerator(registerInfo.ClientInclusion)
        }
        const allInfos = {...registerInfo, ...thePin}
        var pool = await mssql.connect(config);
        var client = await pool.request().query(
          `INSERT INTO ADM_Client (
            ClientName, ClientActive, ClientCNPJ,
            ClientAddress, ClientAddressComplement, ClientAddressNum, ClientBairro, 
            ClientCity, ClientUF, ClientPIN, ClientCEP, ClientTel, ClientMail, ClientInclusion) VALUES (
            '${allInfos.ClientName}', '${allInfos.ClientActive}', '${allInfos.ClientCNPJ}',
            '${allInfos.ClientAddress}', '${allInfos.ClientAddressComplement}', '${allInfos.ClientAddressNum}', 
            '${allInfos.ClientBairro}', '${allInfos.ClientCity}', '${allInfos.ClientUF}', 
            '${allInfos.ClientPIN}', '${allInfos.ClientCEP}', '${allInfos.ClientTel}', 
            '${allInfos.ClientMail}', '${allInfos.ClientInclusion}')`
        );
        emailSender.sendClientRegisterMail(allInfos.ClientMail, allInfos.ClientPIN, allInfos.ClientName)
        return client;
    } catch (error) {
        console.log(error);
    }
}

const driverRegistration = async(registerDrvrInfo) => {
    try {
        const theDriverPin = {
            ColabPIN_ID : pinGenerator.driverPinGenerator()
        }
        const driverInfos = {...registerDrvrInfo, ...theDriverPin}
        var pool = await mssql.connect(config);
        var driver = await pool.request().query(
            `INSERT INTO ADM_Colab (
                ColabName, ColabSurname, ColabTel, 
                ColabEmail, ColabRole_ID, ColabPIN_ID,
                ColabClientID, ColabCNH,
                ColabCNH_Val_date, ColabCPF, Colab_Active,
                ColabInclusion_Date ) VALUES (
                '${driverInfos.ColabName}', '${driverInfos.ColabSurname}', '${driverInfos.ColabTel}',
                '${driverInfos.ColabEmail}', '${driverInfos.ColabRole_ID}', 
                '${driverInfos.ColabPIN_ID}', '${driverInfos.ColabClientID}', '${driverInfos.ColabCNH}',
                '${driverInfos.ColabCNH_Val_date}', '${driverInfos.ColabCPF}', '${driverInfos.Colab_Active}',
                '${driverInfos.ColabInclusion_Date}')`
        );
        emailSender.sendDriverRegisterMail(driverInfos.ColabEmail, driverInfos.ColabPIN_ID, driverInfos.ColabName)
        return driver;
    } catch (error) {
        console.log(error);
    }
}



module.exports = {
    getClientByAPI,
    getColabByAPI,
    startJourney,
    finishJourney,
    startMacro,
    getOldJourneys,
    getPastWorkDays,
    startWorkDay,
    finishWorkDay,
    startLunch,
    finishLunch,
    trackInsertion,
    clientTestRegistration,
    driverRegistration
}