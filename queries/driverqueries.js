const config = require('./dbConfig');
const mssql = require('mssql');
const pinGenerator = require('../utils/pinGenerator');
const emailSender = require('../utils/emailSender');

const loadColabRoles = async() => {
  try {
    console.log("acerto")
    var pool = await mssql.connect(config)
    var clients = pool.request().query(`SELECT * from ADM_Roles`);
    return clients;
  } catch (error) {
    console.log(error);
  }
}

const colabInsertion = async( registerColabObj ) => {
    try {
      /** gera o PIN aleatório */
      const theDriverPin = {
        ColabPIN_ID : pinGenerator.driverPinGenerator()
      }
      const colabObj = {...registerColabObj, ...theDriverPin}
      console.log(colabObj);
      var pool = await mssql.connect(config);
      var admin = await pool.request().query(
        `INSERT INTO ADM_Colab (
          ColabName, ColabSurname, ColabTel, 
          ColabEmail, ColabRole_ID, ColabPIN_ID,
          ColabClientID, ColabAddress, ColabAddressComplement,
          ColabAddressNum, ColabCity, ColabUF, ColabCep, ColabCNH,
          ColabCNH_Val_date, ColabCPF, Colab_Active, ColabCalendar_User,
          ColabInclusion_Date, ColabMonth_Val ) VALUES (
          '${colabObj.DriverName}', '${colabObj.DriverSurname}', '${colabObj.DriverTel}',
          '${colabObj.DriverEmail}', '${colabObj.DriverRole_ID}', 
          '${colabObj.ColabPIN_ID}', '${colabObj.DriverClientID}', '${colabObj.DriverAddress}',
          '${colabObj.DriverAddressComplement}', '${colabObj.DriverAddressNumber}', '${colabObj.DriverCity}',
          '${colabObj.DriverUF}', '${colabObj.DriverCep}', '${colabObj.DriverCNH}',
          '${colabObj.DriverCNH_Val_date}', '${colabObj.DriverCPF}', '${colabObj.Driver_Active}',
          '${colabObj.DriverCalendar_User}', '${colabObj.DriverInclusion_Date}', '${colabObj.DriverMonth_Val}')`
      );
      emailSender.sendDriverRegisterMail(colabObj.DriverEmail, colabObj.ColabPIN_ID, colabObj.DriverName)
      return admin;
    } catch (error) {
      console.log(error);
    }
  }

  const loadColaborators = async( bossID ) => {
    try {
      console.log(bossID)
      var pool = await mssql.connect(config)
      var workers = pool.request().query(`SELECT * from ADM_Colab WHERE ColabClientID = ${bossID.clientID}`);
      return workers;
    } catch (error) {
      console.log(error);      
    }
  }

  const loadTheColaborator = async( guy ) => {
    try {
      console.log(guy);
      var pool = await mssql.connect(config);
      var worker = await pool.request().query(`SELECT * FROM ADM_Colab WHERE ColabID = ${guy.ColabID}`);
      return worker;
    } catch (error) {
      console.log(error);
    }
  }

  const suspendTheColaborator = async( guy ) => {
    try {
      console.log(guy);
      var pool = await mssql.connect(config);
      var worker = await pool.request().query(
        `UPDATE ADM_Colab SET
         Colab_Active='${false}'
         WHERE ColabID = ${guy.ColabID}`);
      return worker;
    } catch (error) {
      console.log(error);
    }
  }

  const updateColaborator = async( colabObj ) => {
    try {
      var pool = await mssql.connect(config);
      var workersolo = await pool.request().query(
        `UPDATE ADM_Colab SET
          ColabName='${colabObj.ColabName}',
          ColabSurname='${colabObj.ColabSurname}',
          ColabTel='${colabObj.ColabTel}',
          ColabRole_ID='${colabObj.ColabRole_ID}',
          ColabAddress='${colabObj.ColabAddress}', 
          ColabAddressComplement='${colabObj.ColabAddressComplement}',
          ColabAddressNum='${colabObj.ColabAddressNum}',
          ColabCity='${colabObj.ColabCity}', 
          ColabUF='${colabObj.ColabUF}',
          ColabCep='${colabObj.ColabCep}',
          ColabCNH='${colabObj.ColabCNH}',
          ColabCNH_Val_date='${colabObj.ColabCNH_Val_date}',
          Colab_Active='${colabObj.Colab_Active}', 
          ColabCalendar_User='${colabObj.ColabCalendar_User}',
          ColabInclusion_Date='${colabObj.ColabInclusion_Date}',
          ColabMonth_Val='${colabObj.ColabMonth_Val}'
          WHERE ColabID = '${colabObj.ColabID}'`
      );
      console.log(workersolo);
      return workersolo;
    } catch (error) {
      console.log(error);
    }
  } 

  const deleteColaborator = async( bye ) => {
    try {
      var pool = await mssql.connect(config);
      var worker = await pool.request().query(
        `DELETE FROM ADM_Colab WHERE ColabID = '${bye.ColabID}'`
      );
      return worker;
    } catch (error) {
      console.log(error);
    }
  } 

module.exports = {
    colabInsertion,
    loadColabRoles,
    loadColaborators,
    deleteColaborator,
    loadTheColaborator,
    suspendTheColaborator,
    updateColaborator
}