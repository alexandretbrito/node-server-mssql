const config = require('./dbConfig');
const mssql = require('mssql');
const bcrypt = require('bcrypt');


  /// CLIENTES

  const loadClients = async() => {
    try {
      var pool = await mssql.connect(config)
      var clients = pool.request().query("SELECT * from ADM_Client");
      return clients;
    } catch (error) {
      console.log("errou");
      console.log(error);
    }
  }

  const loadClientsDetails = async() => {
    try {
      var pool = await mssql.connect(config)
      var clients = pool.request().query(`SELECT ClientID, ClientName, ClientCNPJ, ClientActive, ClientCity, ClientUF,
      (select Count(ManagerClientID) from ADM_Manager  where ADM_Manager.ManagerClientID=ADM_Client.ClientID) as Managers,(select Count(ColabClientID) from ADM_Colab  where ADM_Colab.ColabClientID=ADM_Client.ClientID) as Drivers 
      FROM ADM_Client`);
      return clients;
    } catch (error) {
      console.log(error);
    }
  }  

  const getClient = async( bye ) => {
    try {
      console.log(bye);
      var pool = await mssql.connect(config);
      var admin = await pool.request().query(
        `SELECT ClientName, ClientActive FROM ADM_Client WHERE ClientID = '${bye.ColabClientID}'`
      );
      return admin;
    } catch (error) {
      console.log(error);
    }
  }

  const deleteClient = async( bye ) => {
    try {
      var pool = await mssql.connect(config);
      var admin = await pool.request().query(
        `DELETE FROM ADM_Client WHERE ClientID = '${bye.ClientID}'`
      );
      return admin;
    } catch (error) {
      console.log(error);
    }
  } 

  const clientInsertion = async( clientObj ) => {
    try {
      var pool = await mssql.connect(config);
      var client = await pool.request().query(
        `INSERT INTO ADM_Client (
          ClientName, ClientActive, ClientCNPJ,
          ClientAddress, ClientAddressComplement, ClientAddressNum, ClientBairro, 
          ClientCity, ClientUF, ClientPIN, ClientCEP) VALUES (
          '${clientObj.ClientName}', '${clientObj.ClientActive}', '${clientObj.ClientCNPJ}',
          '${clientObj.ClientAddress}', '${clientObj.ClientAddressComplement}', '${clientObj.ClientAddressNum}', 
          '${clientObj.ClientBairro}', '${clientObj.ClientCity}', '${clientObj.ClientUF}', 
          '${clientObj.ClientPIN}', '${clientObj.ClientCEP}')`
      );
      return client;
    } catch (error) {
      console.log(error);
    }
  }

  module.exports = {
    clientInsertion,
    getClient,
    deleteClient,
    loadClientsDetails,
    loadClients
}