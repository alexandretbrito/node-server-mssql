const express = require('express');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const dbChain = require('./queries/queries');
const bcrypt = require('bcrypt');
const tokenMngr = require('./utils/tokenManager');

const app = express();
app.use(express.json());
app.use(cors());

const authRoutes = require('./routes/authRoutes');
const colabRoutes = require('./routes/colabRoutes');
const SuperAdMinRoutes = require('./routes/SuperAdmRoutes')
const clientRoutes = require('./routes/clientRoutes')
const managerRoutes = require('./routes/managerRoutes')
const appRoutes = require('./routes/appRoutes')

/// AUTH
app.use('/auth', authRoutes);
/// DRIVER
app.use('/colaborador', colabRoutes);
/// SUPER ADMINISTRADORES
app.use('/', SuperAdMinRoutes);
/// CLIENTES
app.use('/', clientRoutes);
/// ADMINISTRADORES
app.use('/', managerRoutes);
/// ROLES
app.use('/app', appRoutes);
/// APP

app.get('/roles', async(req, res, next) => {
    const result = await dbChain.loadRoles() 
    res.send(result)
})

/// ADMINISTRADORES

app.get('/listaadmin', async(req, res, next) => {
    const result = await dbChain.loadManagers() 
    res.send(result)
})


app.post('/insereadmin', async(req, res) => {
    const result = await dbChain.adminInsertion(req.body);
    if(result)  {
        res.send(result)
    }else{
        console.log(req.body)
        res.send({data: "Um erro aconteceu"})
    }
})


app.listen( 5000, () => console.log(`ouvindo ok`) )